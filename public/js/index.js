$(function() {
    $("#draggable li").draggable({
        containment : 'document',
        revert : true,
        helper : 'clone',
    });
    $("#droppable_div").droppable({
        hoverClass : 'border',
        drop: function(event, ui) {
            var element = ui.draggable.attr("data-type");
            getComponent(element);
        }
    });
    $(".sortable").sortable();
});

$("#builder_form_reset").on('click', function(){
    $('#form_name').text('');
    $("#droppable :not(#form_name)").empty();
});

function getComponent(element) {
    $.ajax({
        url: 'get_form',
        type: "POST",
        data: {
            view: element,
            _token: $('meta[name="csrf-token"]').attr("content")
        },
        dataType: "JSON",
        success: function (data) {
            openModal(data);
        },
        error: function (xhr, status, error) {
            console.log(error);
            console.log(status);
        },
    });
}

function openModal(data) {
    $("#modal_component").modal();
    $(".modal-body").html(data);
}

$('#modal_form_submit').on('click', function(e) {
    e.preventDefault();
    var element = $('#element').val();
    if (element == 'input') {
        input();
    }
    if (element == 'radio') {
        radio();
    }
    if (element == 'combo') {
        combo();
    }
    if (element == 'text') {
        text();
    }
});

$('#builder_form_btn').on('click', function(e) {
    e.preventDefault();

    var form_name = $('#form_name').val();
    
    $(this).attr('disabled', true);
    $.ajax({
        url: "save_form",
        type: "POST",
        data: {
            form_name: form_name,
            _token: $('meta[name="csrf-token"]').attr("content")
        },
        dataType: "JSON",
        success: function (data) {
            console.log(data);
            if (data.success) {
                $('#successMsg').show();
                $('#successMsg').text(data.success);
                $('#form_name').val('');
                $("#droppable :not(#form_name)").empty();
                setTimeout(function() {
                    $('#successMsg').hide();
                    $('#builder_form_btn').attr('disabled', false);
                }, 2000);
            } else {
                console.log(data.error);
                $('#form_name').val('');
                $('#errorMsg').show();
                $('#errorMsg').text(data.error);
                setTimeout(function() {
                    $('#errorMsg').hide();
                    $('#builder_form_btn').attr('disabled', false);
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
            console.log(status);
            $('#errorMsg').show();
            $('#errorMsg').text(error);
            setTimeout(function() {
                $('#errorMsg').hide();
                $('#builder_form_btn').attr('disabled', false);
            }, 2000);
        },
    });
    $('#form_name').val('');
    $("#droppable :not(#form_name)").empty();
});

function input() {
    var name = $('#name').val();
    var label = $('#label').val();
    var type = $('#type').val();
    var placeholder = $('#placeholder').val();
    
    if (name == '' || label == '' || type == '' || placeholder == '') {
        $.each({'name': name, 'label': label, 'type': type, 'placeholder': placeholder}, function(key, value) {
            if (value == '') {
                $('#'+key+'_error').text('This Field is Required');
            } else {
                $('#'+key+'_error').text('');
            }
        });
    } else {
        var find_name = $('#droppable').find('#'+name+'');
        var find_label = $('#droppable').find('#'+label+'');
        var find_placeholder = $('#droppable').find('#'+placeholder+'');
    
        if (find_name.length == 0 || find_label == 0 || find_placeholder == 0) {
            $.ajax({
                url: "input",
                type: "POST",
                data: {
                    id: name, 
                    name: name, 
                    type: type, 
                    label: label, 
                    placeholder: placeholder, 
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "JSON",
                success: function (data) {
                    $("#droppable").append(data);
                    $('#modal_component').modal('toggle');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    console.log(status);
                },
            });
        } else {
            alert('Field name already exists.!');
            $("#modal_form").closest('form').find("input[type=text], textarea, select").val("");
        }
    }

}

function radio() {
    var name = $('#name').val();
    var label = $('#label').val();
    var value = $('#value').val();

    if (name == '' || label == '' || value == '') {
        $.each({'name': name, 'label': label, 'value': value}, function(key, value) {
            if (value == '') {
                $('#'+key+'_error').text('This Field is Required');
            } else {
                $('#'+key+'_error').text('');
            }
        });
    } else {
        var find_value = $('#droppable').find('#'+value+'');
        var find_label = $('#droppable').find('#'+label+'');

        if (find_value.length == 0 || find_label == 0) {
            $.ajax({
                url: "radio",
                type: "POST",
                data: {
                    id: name, 
                    name: name, 
                    value: value, 
                    type: 'radio', 
                    label: label, 
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "JSON",
                success: function (data) {
                    $("#droppable").append(data);
                    $('#modal_component').modal('toggle');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    console.log(status);
                },
            });
        } else {
            alert('Field name already exists.!');
            $("#modal_form").closest('form').find("input[type=text], textarea, select").val("");
        }
    }
}

function combo() {
    var text = $('input[name^=text]').map(function(idx, elem) {
        return $(elem).val();
    }).get();
    var value = $('input[name^=value]').map(function(idx, elem) {
        return $(elem).val();
    }).get();

    var name = $('#name').val();
    var label = $('#label').val();

    if (name == '' || label == '' || text == '' || value == '') {
        $.each({'name': name, 'label': label, 'text': text, 'value': value}, function(key, value) {
            if (value == '') {
                $('#'+key+'_error').text('This Field is Required');
            } else {
                $('#'+key+'_error').text('');
            }
        });
    } else {
        var find_name = $('#droppable').find('#'+name+'');
        var find_label = $('#droppable').find('#'+label+'');

        if (find_name.length == 0 || find_label == 0) {
            $.ajax({
                url: "combo",
                type: "POST",
                data: {
                    id: name, 
                    name: name, 
                    type: 'combo', 
                    label: label, 
                    values: value, 
                    texts: text, 
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "JSON",
                success: function (data) {
                    $("#droppable").append(data);
                    $('#modal_component').modal('toggle');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    console.log(status);
                },
            });
        } else {
            alert('Field name already exists.!');
            $("#modal_form").closest('form').find("input[type=text], textarea, select").val("");
        }
    }
}

function text() {
    var name = $('#name').val();
    var label = $('#label').val();

    if (name == '' || label == '') {
        $.each({'name': name, 'label': label}, function(key, value) {
            if (value == '') {
                $('#'+key+'_error').text('This Field is Required');
            } else {
                $('#'+key+'_error').text('');
            }
        });
    } else {
        var find_name = $('#droppable').find('#'+name+'');
        var find_label = $('#droppable').find('#'+label+'');

        if (find_name.length == 0 || find_label == 0) {
            $.ajax({
                url: "text",
                type: "POST",
                data: {
                    id: name, 
                    name: name, 
                    type: 'textaria', 
                    label: label, 
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "JSON",
                success: function (data) {
                    $("#droppable").append(data);
                    $('#modal_component').modal('toggle');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    console.log(status);
                },
            });
        } else {
            alert('Field name already exists.!');
            $("#modal_form").closest('form').find("input[type=text], textarea, select").val("");
        }
    }
}
