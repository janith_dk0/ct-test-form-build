$('#builder_form_save_btn').on('click', function(e) {
    e.preventDefault();

    $(this).attr('disabled', true);
    var $inputs = $('#builder_save_form :input');
    var values = {};
    $inputs.each(function() {
        values[this.name] = $(this).val();
    });

    $.each(values, function(key, value) {
        if (value == '') {
            $('#'+key+'_error').text('This Field is Required');
        } else {
            $('#'+key+'_error').text('');
        }
    });

    $.ajax({
        url: "/formbuild/build",
        type: "POST",
        data: {
            data: values,
            _token: $('meta[name="csrf-token"]').attr("content")
        },
        dataType: "JSON",
        success: function (data) {
            console.log(data);
            if (data.success) {
                $('#successMsg').show();
                $('#successMsg').text(data.success);
                setTimeout(function() {
                    $('#successMsg').hide();
                    $('#builder_form_save_btn').attr('disabled', false);
                }, 2000);
            } else {
                console.log(data.error);
                $('#errorMsg').show();
                $('#errorMsg').text(data.error);
                setTimeout(function() {
                    $('#errorMsg').hide();
                    $('#builder_form_save_btn').attr('disabled', false);
                }, 2000);
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
            console.log(status);
            $('#errorMsg').show();
            $('#errorMsg').text(error);
            setTimeout(function() {
                $('#errorMsg').hide();
                $('#builder_form_save_btn').attr('disabled', false);
            }, 2000);
        },
    });
});