<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('formbuild')->group(function () {
    Route::post('/input', [App\Http\Controllers\ComponenetController::class, 'inputComponent']);
    Route::post('/radio', [App\Http\Controllers\ComponenetController::class, 'radioComponent']);
    Route::post('/combo', [App\Http\Controllers\ComponenetController::class, 'comboBoxComponent']);
    Route::post('/text', [App\Http\Controllers\ComponenetController::class, 'textAriaComponent']);
    Route::post('/get_form', [App\Http\Controllers\ComponenetController::class, 'getForm']);
    Route::post('/save_form', [App\Http\Controllers\ComponenetController::class, 'saveForm']);
});