@extends('layouts.master')

@section('css')
@endsection

@section('header')
    Form View
@endsection

@section('header_description')
    View
@endsection

@section('breadcrumb_level')
    Dashboard
@endsection

@section('breadcrumb_here')
    Builder View
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Forms</h3>
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked" id="draggable">
                    @foreach ($views as $view)
                        <li data-form_id="{{$view->id}}"><a href="{{ route('build.show', $view->id) }}"><i class="fa fa-file"></i> {{$view->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

</div>
@endsection
@section('js')
    <script src="{{ asset('js/index.js') }}"></script>
@endsection