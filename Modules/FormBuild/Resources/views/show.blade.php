@extends('layouts.master')

@section('css')
@endsection

@section('header')
    Form Show
@endsection

@section('header_description')
    {{$view->name}}
@endsection

@section('breadcrumb_level')
    Dashboard
@endsection

@section('breadcrumb_here')
    Builder {{$view->name}}
@endsection

@section('content')
<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{$view->name}}</h3>
            </div>
            <div class="alert alert-success" role="alert" id="successMsg" style="display: none"></div>
            <div class="alert alert-danger" role="alert" id="errorMsg" style="display: none"></div>
            <div class="box-body">
                <form action="" method="POST" role="form" id="builder_save_form" enctype="multipart/form-data">
                    @php
                        $sort_order = json_decode($view->component_sort_order, true);
                        $form_component_data = json_decode($view->form_component_data->component_data, true);
                    @endphp
                    @foreach ($form_component_data as $key => $components)
                        @if (strtok($key, "_") == 'text' || strtok($key, "_") == 'date') 
                            <x-formBuild.input id="{{ $components['id'] }}" type="{{ $components['type'] }}" name="{{ $components['name'] }}" placeholder="{{ $components['placeholder'] }}" label="{{ $components['label'] }}" />
                        @endif
                        @if (strtok($key, "_") == 'radio') 
                            <x-formBuild.radio id="{{ $components['id'] }}" name="{{ $components['name'] }}" value="{{ $components['value'] }}" label="{{ $components['label'] }}" />
                        @endif
                        @if (strtok($key, "_") == 'textaria') 
                            <x-formBuild.text-aria id="{{ $components['id'] }}" name="{{ $components['name'] }}" label="{{ $components['label'] }}" />
                        @endif
                        @if (strtok($key, "_") == 'combo') 
                            <x-formBuild.combo-box id="{{ $components['id'] }}" name="{{ $components['name'] }}" label="{{ $components['label'] }}" :texts="array_values($components['texts'])" :values="array_values($components['values'])" ></x-formBuild.combo-box>
                        @endif
                    @endforeach
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" id="builder_form_save_btn" class="btn btn-primary"><i class="fa fa-save "></i> Save</button>
                        </div>
                        <button type="reset" class="btn btn-default" id="builder_form_save_reset"><i class="fa fa-times"></i> Discard</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{ asset('js/show.js') }}"></script>
@endsection