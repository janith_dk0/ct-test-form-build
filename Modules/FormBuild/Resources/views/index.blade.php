@extends('layouts.master')

@section('css')
@endsection

@section('header')
    Form Builder
@endsection

@section('header_description')
    Index
@endsection

@section('breadcrumb_level')
    Dashboard
@endsection

@section('breadcrumb_here')
    Builder Index
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Tool Bar</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked" id="draggable">
                        <li data-type="input"><a href="" onclick="event.preventDefault()"><i class="fa fa-font"></i> Input</a></li>
                        <li data-type="text"><a href="" onclick="event.preventDefault()"><i class="fa fa-keyboard-o"></i> Textarea</a></li>
                        <li data-type="combo"><a href="" onclick="event.preventDefault()"><i class="fa fa-list-ul"></i> Combo Box</a></li>
                        {{-- <li data-type="input"><a href=""><i class="fa fa-calendar-plus-o"></i> Date Picker</a></li> --}}
                        <li data-type="radio"><a href="" onclick="event.preventDefault()"><i class="fa fa-genderless"></i> Radio</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-8" id="droppable_div">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Dynamic Form</h3>
                </div>
                <div class="alert alert-success" role="alert" id="successMsg" style="display: none"></div>
                <div class="alert alert-danger" role="alert" id="errorMsg" style="display: none"></div>
                <div class="box-body">
                    <form action="" method="POST" role="form" id="builder_form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group exept_this">
                            <label for="form_name" class="col-form-label text-md-end">Name of the Form * : </label>
                            <input id="form_name" name="form_name" class="form-control exept_this" type="text" placeholder="Enter Name"></input>
                        </div>
                        <hr>
                        <div id="droppable">
                        </div>
                        <div class="box-footer">
                            <div class="pull-right">
                                <button type="submit" id="builder_form_btn" class="btn btn-primary"><i class="fa fa-save "></i> Save</button>
                            </div>
                            <button type="reset" class="btn btn-default" id="builder_form_reset"><i class="fa fa-times"></i> Discard</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script src="{{ asset('js/index.js') }}"></script>
@endsection