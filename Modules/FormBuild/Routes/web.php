<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('formbuild')->group(function() {
    Route::resource('build', 'FormBuildController')->middleware('auth');
    Route::get('view', 'FormBuildController@view')->middleware('auth')->name('view');
});
