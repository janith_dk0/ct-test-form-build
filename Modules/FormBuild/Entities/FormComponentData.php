<?php

namespace Modules\FormBuild\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormComponentData extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\FormBuild\Database\factories\FormComponentDataFactory::new();
    }

    public function user() {
        return $this->belongsTo(App\Models\User::class);
    }

    public function form_order() {
        return $this->belongsTo(FormOrder::class);
    }
}
