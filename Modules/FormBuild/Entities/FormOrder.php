<?php

namespace Modules\FormBuild\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormOrder extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\FormBuild\Database\factories\FormOrderFactory::new();
    }

    public function user() {
        return $this->belongsTo(App\Models\User::class);
    }

    public function form_component_data() {
        return $this->hasOne(FormComponentData::class);
    }

    public function form_data() {
        return $this->hasMany(FormData::class);
    }
}
