<div class="form-group sortable">
    <label for="{{$id}}" class="col-form-label text-md-end">{{$label}}</label>
    <input id="{{$id}}" type="radio" name="{{$name}}" value="{{$value}}">
    <span class="text-danger" id="{{$id}}"></span>
</div>