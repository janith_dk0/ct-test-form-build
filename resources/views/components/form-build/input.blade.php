<div class="form-group sortable">
    <label for="{{$id}}" class="col-form-label text-md-end">{{$label}}</label>
    <input id="{{$id}}" type="{{$type}}" class="form-control" name="{{$name}}" placeholder="{{$placeholder}}" value="">
    <span class="text-danger" id="{{$id}}_error"></span>
</div>