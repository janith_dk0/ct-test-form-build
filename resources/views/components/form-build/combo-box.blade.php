<div class="form-group sortable">
    <label for="{{$id}}" class="col-form-label text-md-end">{{$label}}</label>
    <select class="form-control" id="{{$id}}" name="{{$name}}" data-parsley-required="true">
        <option value="">Select Option</option>
        @foreach ($values as $key => $value)
            <option value="{{$value}}">{{$texts[$key]}}</option>
        @endforeach
    </select>
    <span class="text-danger" id="{{$id}}_error"></span>
</div>