<div class="form-group sortable">
    <label for="{{$id}}" class="col-form-label text-md-end sortable">{{$label}}</label>
    <textarea id="{{$id}}" name="{{$name}}" rows="4" cols="50"></textarea>
    <span class="text-danger" id="{{$id}}_error"></span>
</div>