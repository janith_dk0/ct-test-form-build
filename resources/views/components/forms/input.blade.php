<input type="hidden" name="element" value="input" id="element">
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="name">Name * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" value="" type="text" id="name" name="name" placeholder="Input Name" data-parsley-required="true" required/>

        @if ($errors->has('name'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('name') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="name_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="label">Label * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('label') ? ' parsley-error' : '' }}" value="" type="text" id="label" name="label" placeholder="Label Name" data-parsley-required="true" />

        @if ($errors->has('label'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('label') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="label_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="placeholder">Placeholder * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('placeholder') ? ' parsley-error' : '' }}" value="" type="text" id="placeholder" name="placeholder" placeholder="Placeholder Text" data-parsley-required="true" />

        @if ($errors->has('placeholder'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('placeholder') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="placeholder_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="type">Types * :</label>
    <div class="col-md-8 col-sm-8">
        <select class="form-control" id="type" name="type" data-parsley-required="true">
            <option value="">Select Department</option>
            <option value="text">Text</option>
            <option value="date">Date Picker</option>
        </select>

        @if ($errors->has('type'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('type') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="type_error"></span>
    </div>
</div>