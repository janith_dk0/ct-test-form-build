<input type="hidden" name="element" value="combo" id="element">
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="name">Name * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" value="" type="text" id="name" name="name" placeholder="Input Name" data-parsley-required="true" required/>

        @if ($errors->has('name'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('name') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="name_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="label">Label * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('label') ? ' parsley-error' : '' }}" value="" type="text" id="label" name="label" placeholder="Label Name" data-parsley-required="true" />

        @if ($errors->has('label'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('label') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="label_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-6 col-sm-6 col-form-label" for="text">Text * :</label>
    <label class="col-md-6 col-sm-6 col-form-label" for="value">Value * :</label>
    <div class="dup_row">
        <div class="col-md-5 col-sm-5">
            <input class="form-control {{ $errors->has('text') ? ' parsley-error' : '' }}" value="" type="text" id="text[0]" name="text[0]" placeholder="Text" data-parsley-required="true" />

            @if ($errors->has('text'))
                <ul class="parsley-errors-list filled" id="parsley-id-5">
                    <li class="parsley-required">{{ $errors->first('text') }}</li>
                </ul>
            @endif
            <span class="text-danger" id="text_error"></span>
        </div>
        <div class="col-md-5 col-sm-5">
            <input class="form-control {{ $errors->has('Value') ? ' parsley-error' : '' }}" value="" type="text" id="value[0]" name="value[0]" placeholder="Value" data-parsley-required="true" />

            @if ($errors->has('value'))
                <ul class="parsley-errors-list filled" id="parsley-id-5">
                    <li class="parsley-required">{{ $errors->first('value') }}</li>
                </ul>
            @endif
            <span class="text-danger" id="value_error"></span>
        </div>
        <div class="col-md-2 col-sm-2">
            <button type="button" id="val_add_button" class="btn btn-default"> Add</button>
        </div>
    </div>
</div>

<script>
    var count = 0;

    $('#val_add_button').on('click', function() {
        count += 1
        $(".dup_row").append(
            '<div class="col-md-5 col-sm-5 appended_'+count+'" style="padding-top: 5px">'+
                '<input class="form-control" value="" type="text" id="text['+count+']" name="text['+count+']" placeholder="Text" data-parsley-required="true" />'+
                '<span class="text-danger" id="text_error_['+count+']"></span>'+
            '</div>'+
            '<div class="col-md-5 col-sm-5 appended_'+count+'" style="padding-top: 5px">'+
                '<input class="form-control" value="" type="text" id="value['+count+']" name="value['+count+']" placeholder="Value" data-parsley-required="true" />'+
                '<span class="text-danger" id="value_error_['+count+']"></span>'+
            '</div>'+
            '<div class="col-md-2 col-sm-2 appended_'+count+'" style="padding-top: 5px">'+
                '<button type="button" id="val_add_button" class="btn btn-danger" onClick="fieldRemove('+count+')"> &nbsp&nbspX&nbsp&nbsp</button>'+
            '</div>'
        );
    });

    function fieldRemove(count) {
        $('.appended_'+count+'').remove();
    }
</script>