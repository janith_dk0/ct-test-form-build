<input type="hidden" name="element" value="text" id="element">
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="name">Name * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" value="" type="text" id="name" name="name" placeholder="Input Name" data-parsley-required="true" required/>

        @if ($errors->has('name'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('name') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="name_error"></span>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-md-4 col-sm-4 col-form-label" for="label">Label * :</label>
    <div class="col-md-8 col-sm-8">
        <input class="form-control {{ $errors->has('label') ? ' parsley-error' : '' }}" value="" type="text" id="label" name="label" placeholder="Label Name" data-parsley-required="true" />

        @if ($errors->has('label'))
            <ul class="parsley-errors-list filled" id="parsley-id-5">
                <li class="parsley-required">{{ $errors->first('label') }}</li>
            </ul>
        @endif
        <span class="text-danger" id="label_error"></span>
    </div>
</div>