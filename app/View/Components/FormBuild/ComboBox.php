<?php

namespace App\View\Components\FormBuild;

use Illuminate\View\Component;

class ComboBox extends Component
{
    public $id;
    public $name;
    public $label;
    public $texts;
    public $values;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $name, $label, $texts, $values)
    {
        $this->id = $id;
        $this->name = $name;
        $this->label = $label;
        $this->texts = $texts;
        $this->values = $values;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form-build.combo-box');
    }
}
