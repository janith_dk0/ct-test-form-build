<?php

namespace App\View\Components\FormBuild;

use Illuminate\View\Component;

class Input extends Component
{
    public $id;
    public $name;
    public $type;
    public $label;
    public $placeholder;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $name, $type, $label, $placeholder)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->label = $label;
        $this->placeholder = $placeholder;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form-build.input');
    }
}
