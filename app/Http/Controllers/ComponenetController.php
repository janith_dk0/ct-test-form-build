<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\FormBuild\Entities\FormOrder;
use Modules\FormBuild\Entities\FormComponentData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use View;

class ComponenetController extends Controller
{
    public function inputComponent(Request $request) {

        $validator = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'type' => 'required',
            'label' => 'required',
            'placeholder' => 'required',
        ]);

        $id = str_replace(' ', '_', strtolower($request->id));
        $name = str_replace(' ', '_', strtolower($request->name));
        $type = $request->type;
        $label = $request->label;
        $placeholder = $request->placeholder;

        $request->merge([
            'id' => $id,
            'name' => $name,
        ]);

        $this->addToCache($request);

        return response()->json([
            View::make('components.form-build.input', compact('id', 'name', 'type', 'label', 'placeholder'))->render()
        ]);
    }

    public function radioComponent(Request $request) {

        $validator = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'label' => 'required',
            'value' => 'required',
        ]);

        $id = str_replace(' ', '_', strtolower($request->id));
        $name = str_replace(' ', '_', strtolower($request->name));
        $label = $request->label;
        $value = $request->value;

        $request->merge([
            'id' => $id,
            'name' => $name,
        ]);

        $this->addToCache($request);

        return response()->json([
            View::make('components.form-build.radio', compact('id', 'name', 'label', 'value'))->render()
        ]);
    }

    public function comboBoxComponent(Request $request) {

        $validator = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'label' => 'required',
            'texts' => 'required|array',
            'values' => 'required|array',
        ]);

        $id = str_replace(' ', '_', strtolower($request->id));
        $name = str_replace(' ', '_', strtolower($request->name));
        $label = $request->label;
        $texts = $request->texts;
        $values = array_map('strtolower', $request->values);

        $request->merge([
            'id' => $id,
            'name' => $name,
        ]);

        $this->addToCache($request);

        return response()->json([
            View::make('components.form-build.combo-box', compact('id', 'name', 'label', 'texts', 'values'))->render()
        ]);
    }

    public function textAriaComponent(Request $request) {

        $validator = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'label' => 'required',
        ]);

        $id = str_replace(' ', '_', strtolower($request->id));
        $name = str_replace(' ', '_', strtolower($request->name));
        $label = $request->label;

        $request->merge([
            'id' => $id,
            'name' => $name,
        ]);

        $this->addToCache($request);

        return response()->json([
            View::make('components.form-build.text-aria', compact('id', 'name', 'label'))->render()
        ]);
    }

    public function getForm(Request $request){
        return response()->json([
            view('components.forms.'.$request->view.'')->render()
        ]);
    }

    public function saveForm(Request $request) {

        $validator = $request->validate([
            'form_name' => 'required',
        ]);
        if (Cache::has('element')) {
            $arr = [];
            $sort_order = [];
            $element = Cache::get('element');
            foreach ($element as $key => $value) {
                $sort_order[$value['id']] = $value['type'];
            }
            DB::transaction(function () use ($request, $sort_order, $element) {
                $form_order = new FormOrder;
                $form_order->name = $request->form_name;
                $form_order->user_id = Auth::user()->id;
                $form_order->component_sort_order = json_encode($sort_order, JSON_FORCE_OBJECT);
                $form_order->save();
                
                $form_comp_data = new FormComponentData;
                $form_comp_data->user_id = Auth::user()->id;
                $form_comp_data->form_order_id = $form_order->id;
                $form_comp_data->component_data = json_encode($element, JSON_FORCE_OBJECT);
                $form_comp_data->save();

                Cache::forget('element');
                Cache::flush();
            });
            return response()->json(['status' => 200, 'success' => 'Form Update Success.!']);
        }
        return response()->json(['status' => 500, 'error' => 'Something Went Wrong.!']);
    }

    public function addToCache(Request $request) {
        $el_arr = [];

        foreach ($request->all() as $key => $value) {
            if ($key != '_token') {
                $el_arr[$key] = $value;
            }
        }

        if (Cache::has('element')) {
            $element = Cache::get('element');
            $element[$request->type.'_'.$request->id] = $el_arr;
            Cache::put('element', $element);
        } else {
            $element = [];
            $element[$request->type.'_'.$request->id] = $el_arr;
            Cache::put('element', $element);
        }
    }
}
